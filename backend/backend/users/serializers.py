from django.contrib.auth import get_user_model
from backend.boofen.models import Comment
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    #comments = serializers.PrimaryKeyRelatedField(many=True, queryset=Comment.objects.all())
    class Meta:
        model = User
        fields = ["username", "name", "url"] #TODO add comments here

        extra_kwargs = {
            "url": {"view_name": "api:user-detail", "lookup_field": "username"}
        }
