from rest_framework import generics
from backend.boofen.models import Boofe
from backend.boofen.serializers import BoofeSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import viewsets

class BoofeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Boofe.objects.all()
    serializer_class = BoofeSerializer