from django.db import models
from backend.users.models import User

class Boofe(models.Model):
    name = models.CharField(max_length=255)
    short_description = models.TextField()
    long_description = models.TextField()
    directions = models.TextField(blank=True)
    longitude = models.CharField(max_length=50, blank=True)
    latitude = models.CharField(max_length=50, blank=True)
    sleeping_places = models.IntegerField(blank=True)
    official = models.BooleanField(blank=True)
    dry_after_rain = models.BooleanField(blank=True)
    sheltered_from_wind = models.BooleanField(blank=True)
    difficulty_to_reach = models.IntegerField(blank=True)
    image = models.ImageField(upload_to='boofen_images/', height_field=None, width_field=None, max_length=None)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Comment (models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    rating = models.IntegerField()
    image = models.ImageField(upload_to='comment_images/', height_field=None, width_field=None, max_length=None)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    boofe = models.ForeignKey(Boofe, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)