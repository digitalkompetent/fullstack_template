from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class BoofenConfig(AppConfig):
    name = 'backend.boofen'
    verbose_name = _("Boofen")

    def ready(self):
        try:
            import backend.boofen.signals  # noqa F401
        except ImportError:
            pass