from django.contrib import admin
from backend.boofen.models import Boofe
from backend.boofen.models import Comment

# Register your models here.
admin.site.register(Boofe)
admin.site.register(Comment)