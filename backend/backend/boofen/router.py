from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from backend.boofen.views import BoofeViewSet

app_name = "boofen"

# Create a router and register our viewsets with it.
if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("", BoofeViewSet)
urlpatterns = router.urls