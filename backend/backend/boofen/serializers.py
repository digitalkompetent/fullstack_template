from rest_framework import serializers
from backend.boofen.models import Boofe
from backend.boofen.models import Comment

class BoofeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Boofe
        fields = [
            'id',
            'name', 
            'short_description', 
            'long_description', 
            'directions', 
            'longitude', 
            'latitude', 
            'sleeping_places',
            'official',
            'dry_after_rain', 
            'image'
            ]

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'title', 
            'rating'
            #TODO add additional fields
        ]