# FULL STACK TEMPLATE for WebApp Projects #

 This repo is a starting point for cutting edge webapps using django and vue.

- 💾 django REST API for backend
- 🖥 vue for frontend
- 🚢 fully dockerized
- ✅ best practice
